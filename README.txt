Configuration Filter
--------------------
Filter out results on admin/config page. The module uses listjs library in the
background to filter out results.


Installation
------------
- Download and install the module in the /modules.
- listjs will be used from a CDN link.
- Enable the module.

OR

Optionally, this module can be used locally (not requiring internet connection)
- Module is fully compatible with the libraries module.
- Create a new directory - sites/all/libraries/listjs.
- Download compressed listjs (list.min.js) library from
  http://www.listjs.com/overview/download
- Place the downloaded library as sites/all/libraries/listjs/list.min.js


Configuration
-------------
- You can toggle between CDN and listjs local usage via
  admin/config/user-interface/config_filter.
- Latest listjs version can be used via CDN link.


Module maintainers
------------------
Gauravjeet singh 'gauravjeet' https://www.drupal.org/u/gauravjeet
Manjit Singh 'Manjit.Singh' https://drupal.org/u/manjit.singh
Zeeshan khan 'zeeshan_khan' https://www.drupal.org/u/zeeshan_khan
