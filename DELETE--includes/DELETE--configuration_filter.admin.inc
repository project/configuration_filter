<?php
/**
 * @file
 * Admin page callback for admin/config page.
 */

/**
 * Menu callback; Provide the administration overview page.
 */
function configuration_filter_admin_config_page() {
  $module_path = drupal_get_path('module', 'configuration_filter');
  $blocks = array();

  // Add libraries.
  if (variable_get('listjs_load_libraries', 'cdn') == 'cdn') {
    $cdn_url = variable_get('listjs_cdn_url', 'https://cdnjs.cloudflare.com/ajax/libs/list.js/1.2.0/list.min.js');
    $cdn_url = check_plain($cdn_url);
    drupal_add_js($cdn_url);
  }
  else {
    if (function_exists('libraries_load')) {
      libraries_load('listjs');
    }
    else {
      drupal_set_message(t('Libraries module is not installed. You may have to change the !settings to use CDN version of list.js library', array('!settings' => l('settings', 'admin/config/user-interface/config_filter'))), 'error');
    }
  }
  drupal_add_css($module_path . '/misc/config_filter.css');
  drupal_add_js($module_path . '/misc/config_filter.js');

  // Check for status report errors.
  if (user_access('administer site configuration')) {
    drupal_set_message(t('One or more problems were detected with your Drupal installation. Check the <a href="@status">status report</a> for more information.', array('@status' => url('admin/reports/status'))), 'error');
  }

  if ($admin = db_query("SELECT menu_name, mlid FROM {menu_links} WHERE link_path = 'admin/config' AND module = 'system'")->fetchAssoc()) {
    $result = db_query("SELECT m.*, ml.* FROM {menu_links} ml INNER JOIN {menu_router} m ON ml.router_path = m.path WHERE ml.link_path <> 'admin/help' AND menu_name = :menu_name AND ml.plid = :mlid AND hidden = 0", $admin, array('fetch' => PDO::FETCH_ASSOC));

    foreach ($result as $item) {
      _menu_link_translate($item);
      if (!$item['access']) {
        continue;
      }

      // The link description.
      if (!empty($item['localized_options']['attributes']['title'])) {
        $item['description'] = $item['localized_options']['attributes']['title'];
        unset($item['localized_options']['attributes']['title']);
      }
      $block = $item;
      $block['content'] = '';
      $block['content'] .= theme('configuration_filter_block_content', array('content' => system_admin_menu_block($item)));
      if (!empty($block['content'])) {
        $block['show'] = TRUE;
      }
      else {
        $block = array('title' => '', 'content' => '');
      }

      // The weight is offset so it is always positive, with a uniform 5-digits.
      $blocks[(50000 + $item['weight']) . ' ' . $item['title'] . ' ' . $item['mlid']] = $block;
    }
  }

  $output = '';
  foreach ($blocks as $block) {
    $output .= $block['content'];
  }

  if ($blocks) {
    ksort($blocks);
    $rendered_blocks = theme('configuration_filter_admin_page', array('blocks' => $blocks));
    return theme('configuration_filter_add_searchbox', array('content' => $rendered_blocks));
  }
  else {
    return t('You do not have any administrative items.');
  }
}

/**
 * Implements theme callback.
 */
function theme_configuration_filter_add_searchbox($variables) {
  $output = '';
  $output .= '<div id="listjs-search">';
  $output .= '<div class="config-filter-search-wrapper">';
  $output .= '<input class="search" placeholder="Search configuration">';
  $output .= theme('system_compact_link');
  $output .= '</div>';
  $output .= $variables['content'];
  $output .= '</div>';
  return $output;
}

/**
 * Implements theme callback.
 */
function theme_configuration_filter_block_content($variables) {
  $content = $variables['content'];
  $output = '';

  if (!empty($content)) {
    foreach ($content as $item) {
      $output .= '<li>';

      $output .= '<div class="admin-panel">';
      $output .= '<div class="body">';
      $output .=  l($item['title'], $item['href'], array(
          'attributes' => array(
            'class' => 'config-filter-list-item',
          )
        )
      );
      $output .= '</div>';
      if (!system_admin_compact_mode() && isset($item['description'])) {
        $output .= '<p class="config-filter-desc">' . filter_xss_admin($item['description']) . '</p>';
      }
      $output .= '</div>';
      $output .= '</li>';
    }
  }
  return $output;
}

/**
 * Implements theme callback.
 */
function theme_configuration_filter_admin_block($variables) {
  $block = $variables['block'];
  $output = '';

  // Don't display the block if it has no content to display.
  if (empty($block['show'])) {
    return $output;
  }
  return $output;
}

/**
 * Implements theme callback.
 */
function theme_configuration_filter_admin_page($variables) {
  $blocks = $variables['blocks'];
  $container = '';
  $output = '';
  $minimal_layout_class = '';
  $class= 'list';

  foreach ($blocks as $block) {
    $container .= '<h3 class="config-filter-title">' . $block['title'] . '</h3>';
    $container .= $block['content'];
  }

  if ($compact = system_admin_compact_mode()) {
    $class .= ' compact';
  }
  else {
    $minimal_layout_class = 'config-filter-show-desc ';
  }
  $output .= '<div class="confing_filter ' . $minimal_layout_class . 'clearfix">';
  $output .= '<ul class="' . $class . '">';
  $output .= $container;
  $output .= '</ul>';
  return $output;
}
