<?php
/**
 * @file
 * File for common configuration methods.
 */

/**
 * Implements hook_form().
 */
function configuration_filter_config($form, &$form_state) {
  $form['listjs_load_libraries'] = array(
    '#type' => 'radios',
    '#title' => t('Attach List.js Library'),
    '#options' => array(
      'cdn' => t('Use CDN'),
      'local' => t('Local')
    ),
    '#default_value' => variable_get('listjs_load_libraries', 'cdn'),
    '#required' => True,
  );
  $form['listjs_cdn_url'] = array(
    '#type' => 'textfield',
    '#title' => t('CDN Listjs url'),
    '#description' => t('Find latest CDN version of List.js here: !link - search "list.min.js" and copy the link here',array('!link' => l('https://cdnjs.com/', 'https://cdnjs.com/'))),
    '#default_value' => variable_get('listjs_cdn_url', 'https://cdnjs.cloudflare.com/ajax/libs/list.js/1.2.0/list.min.js'),
    '#states' => array(
      'visible' => array(
        ':input[name="listjs_load_libraries"]' => array('value' => 'cdn'),
      ),
    ),
    '#required' => True,
  );


  // Check if library list.js library exists.
  $desc = '<div>' . t('!library_link library was not found on the server. To use List.js : <ul><li>Download and install !libraries module.</li> <li>Go to !link and download the Uncompressed version.</li> <li>Place the downloaded list.min.js inside <strong>sites/all/libraries/listjs</strong> folder</li></ul>', array(
      '!libraries' => l('libraries', 'https://www.drupal.org/project/libraries'),
      '!library_link' => l('List.js', 'http://www.listjs.com/'),
      '!link' => l('list.js', 'http://www.listjs.com/overview/download'),
  )) . '</div>';

  if (function_exists('libraries_get_path')) {
    $listjs = libraries_detect('listjs');
    $listjs_path = libraries_get_path('listjs');
    $listjs_files = scandir($listjs_path);

    if ($listjs['installed']) {
      $desc .= '<p>' . t('<strong>List.js library path was found on the server but it may not be installed properly.</strong>') . '</p>';
      if (in_array('list.min.js', $listjs_files)) {
        $desc = '<div>' . t('!link library was found and is successfully installed on the server.', array('!link' => l('List.js', 'http://www.listjs.com/'))) . '</div>';
      }
    }
  }

  $form['listjs_local_url'] = array(
    '#description' => $desc,
    '#type' => 'item',
    '#states' => array(
      'visible' => array(
        ':input[name="listjs_load_libraries"]' => array('value' => 'local'),
      ),
    ),
  );
  return system_settings_form($form);
}