(function ($, Drupal) {
  Drupal.behaviors.configuration_filter = {
    attach: function (context, settings) {

      // Keep focus on textfield.
      $("#listjs-search .search").focus();

      // Initialize list.js object.
      new List('listjs-search', {
        valueNames: [
          'config-filter-desc',
          'config-filter-list-item'
        ]
      });
    }
  }
})(jQuery, Drupal);