<?php

/**
 * @file
 * Contains \Drupal\configuration_filter\Form\ConfigurationForm.
 */

namespace Drupal\configuration_filter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Settings form for configurations page.
 */
class ConfigurationForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'configuration_filter_configuration';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Create a $form API array.
    // This field is use for load libraries.
    $form['listjs_load_libraries'] = array(
      '#type' => 'radios',
      '#title' => t('Attach List.js Library'),
      '#options' => array(
        'cdn' => t('Use CDN'),
        'local' => t('Local')
      ),
      '#default_value' => \Drupal::config('configuration_filter.settings')->get('listjs_load_libraries'),
      '#required' => True,
    );

    // Client side url where libraries are exists.
    $form['listjs_cdn_url'] = array(
      '#type' => 'textfield',
      '#title' => t('CDN Listjs url'),
      '#description' => t('Find latest CDN version of List.js here: !link - search "list.min.js" and copy the link here', array('!link' => \Drupal::l(t('https://cdnjs.com/'), Url::fromUri('https://cdnjs.com/')))),
      '#default_value' => \Drupal::config('configuration_filter.settings')->get('listjs_cdn_url'),
      '#states' => array(
        'visible' => array(
          ':input[name="listjs_load_libraries"]' => array('value' => 'cdn'),
        ),
      ),
      '#required' => True,
    );

    // Check if library list.js library exists.
    $desc = '<div>' . t('!library_link library was not found on the server. To use List.js : <ul><li>Download and install !libraries module.</li> <li>Go to !link and download the Uncompressed version.</li> <li>Place the downloaded list.min.js inside <strong>sites/all/libraries/listjs</strong> folder</li></ul>', array(
          '!libraries' => \Drupal::l(t('libraries'), Url::fromUri('https://www.drupal.org/project/libraries')),
          '!library_link' => \Drupal::l(t('List.js'), Url::fromUri('http://www.listjs.com/')),
          '!link' => \Drupal::l(t('list.js'), Url::fromUri('http://www.listjs.com/overview/download')),
        )) . '</div>';

    if (function_exists('libraries_get_path')) {
      $listjs = libraries_detect('listjs');
      $listjs_path = libraries_get_path('listjs');
      $listjs_files = scandir($listjs_path);

      if ($listjs['installed']) {
        $desc .= '<p>' . t('<strong>List.js library path was found on the server but it may not be installed properly.</strong>') . '</p>';
        if (in_array('list.min.js', $listjs_files)) {
          $desc = '<div>' . t('!link library was found and is successfully installed on the server.', array('!link' => \Drupal::l(t('List.js'), Url::fromUri('http://www.listjs.com/')))) . '</div>';
        }
      }
    }

    $form['listjs_local_url'] = array(
      '#description' => $desc,
      '#type' => 'item',
      '#states' => array(
        'visible' => array(
          ':input[name="listjs_load_libraries"]' => array('value' => 'local'),
        ),
      ),
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
